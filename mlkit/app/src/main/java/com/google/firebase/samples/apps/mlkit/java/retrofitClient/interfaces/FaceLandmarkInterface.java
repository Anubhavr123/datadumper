package com.google.firebase.samples.apps.mlkit.java.retrofitClient.interfaces;

import com.google.firebase.samples.apps.mlkit.java.retrofitClient.models.FaceLandmarkResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface FaceLandmarkInterface {

    @POST("dbsearch")
    Call<FaceLandmarkResponse> loadDataForPage(@Body HashMap<String, Object> body);

    @POST("push")
    Call<FaceLandmarkResponse> pushLandMarksForUrl(@Body HashMap<String, Object> body);

}
