package com.google.firebase.samples.apps.mlkit.java.retrofitClient.clients;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.firebase.samples.apps.mlkit.common.VisionImageProcessor;
import com.google.firebase.samples.apps.mlkit.java.facedetection.FaceContourDetectorProcessor;
import com.google.firebase.samples.apps.mlkit.java.retrofitClient.interceptors.LoggingInterceptor;
import com.google.firebase.samples.apps.mlkit.java.retrofitClient.interfaces.FaceLandmarkInterface;
import com.google.firebase.samples.apps.mlkit.java.retrofitClient.models.FaceLandmarkResponse;
import com.google.firebase.samples.apps.mlkit.java.retrofitClient.utils.BackgroundThreadFactory;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.google.firebase.samples.apps.mlkit.java.retrofitClient.ApiUtils.FACE_LANDMARK_BASE_URL;
import static java.lang.Thread.sleep;

/**
 *
 * Client class for which handles the REST calls as well as the firebase callbacks , when the processing is done.
 *
 * Initialize this class as
 *           final FaceLandmarkClient client = new FaceLandmarkClient.Builder()
 *                                               .setFetchingCollectionName("JDlandmark_faceDb")
 *                                               .setFetchingDbName("JDlandmark_faceDb")
 *                                               .setPageSize(10)
 *                                               .setInitialPageNumber(1)
 *                                               .setDumpingDbName("landmark_dump")
 *                                               .setDumpingCollectionName("landmark_dump")
 *                                               .build();
 *           client.fetchImagesForPage(1);
 *
 */
public class FaceLandmarkClient {

    private static String TAG = "FaceLandmarkClient";

    //REST clients
    private static Retrofit retrofit = null;
    private static FaceLandmarkInterface faceLandmarkInterface=null;

    //Fetching parameters
    private String fetchingDbName;
    private String fetchingCollectionName;
    private int pageSize;
    private int currentPage;

    //Dumping parameters
    private String dumpingDbName;
    private String dumpingCollectionName;

    //Firebase processor
    private VisionImageProcessor imageProcessor;


    //Threading variables
    int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    int KEEP_ALIVE_TIME = 2;
    TimeUnit KEEP_ALIVE_TIME_UNIT =  TimeUnit.SECONDS;

    //Task queue for all the threads
    BlockingQueue<Runnable> taskQueue = new LinkedBlockingQueue<Runnable>();
    //Maintains all the running tasks.
    BlockingQueue<Future> mRunningTaskList = new LinkedBlockingQueue<Future>();

    //Executor service which runs all the threads in a common pool
    ExecutorService executorService = new ThreadPoolExecutor(1,
            NUMBER_OF_CORES*2,
            KEEP_ALIVE_TIME,
            KEEP_ALIVE_TIME_UNIT,
            taskQueue,
            new BackgroundThreadFactory());

    //Number of data points that have been pushed
    //This number appears in the log, so that we can track if the application crashes midway
    private AtomicInteger dataPointPushed = new AtomicInteger(0);

    private WeakReference<Context> mContext;
    /**
     *
     * Builder class for creating a client
     *
     */
    public static class Builder{
        private String fetchingDbName;
        private String fetchingCollectionName;
        private String dumpingDbName;
        private String dumpingCollectionName;
        private int pageSize;
        private int startPage;
        private Context mContext;

        public Builder setFetchingDbName(String fetchingDbName) {
            this.fetchingDbName = fetchingDbName;
            return this;
        }

        public Builder setFetchingCollectionName(String fetchingCollectionName) {
            this.fetchingCollectionName = fetchingCollectionName;
            return this;
        }

        public Builder setPageSize(int pageSize){
            this.pageSize = pageSize;
            return this;
        }

        public Builder setInitialPageNumber(int pageNumber){
            this.startPage = pageNumber;
            return this;
        }

        public Builder setDumpingDbName(String dumpingDbName) {
            this.dumpingDbName = dumpingDbName;
            return this;
        }

        public Builder setDumpingCollectionName(String dumpingCollectionName) {
            this.dumpingCollectionName = dumpingCollectionName;
            return this;
        }

        public Builder setContext(Context mContext){
            this.mContext = mContext;
            return this;
        }

        public FaceLandmarkClient build(){
            return new FaceLandmarkClient(this.fetchingDbName,this.fetchingCollectionName,this.pageSize,this.startPage,this.dumpingDbName,this.dumpingCollectionName,this.mContext);
        }

    }

    public FaceLandmarkClient(String fetchingDbName, String fetchingCollectionName, int pageSize, int startPage, String dumpingDbName, String dumpingCollectionName, Context mContext){
        this.fetchingDbName = fetchingDbName;
        this.fetchingCollectionName = fetchingCollectionName;
        this.pageSize = pageSize;
        this.currentPage = startPage;
        this.dumpingDbName = dumpingDbName;
        this.dumpingCollectionName = dumpingCollectionName;
        this.imageProcessor = new FaceContourDetectorProcessor();

        //Is true when we wish to write to a file.
        //Useful for testing purpose
        if(mContext !=null)
            this.mContext = new WeakReference<>(mContext);

        initializeRetrofitClient();

    }

    //Empty constructor for when we don't want network responses
    public FaceLandmarkClient(){
        this.imageProcessor = new FaceContourDetectorProcessor();
    }


    //Initializes the retrofit client once
    private void initializeRetrofitClient() {
        if(this.retrofit==null) {

            //Creates a logging interceptor for all the network calls that are being made.
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(new LoggingInterceptor())
                    .build();

            //Creates retrofit instance for the first time
            this.retrofit = new Retrofit.Builder()
                    .baseUrl(FACE_LANDMARK_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();

            //Initializes the api interface
            getApiInterface();
        }
    }


    //Gets the Api interface for making the Api Calls
    public FaceLandmarkInterface getApiInterface(){
        if(this.faceLandmarkInterface==null){
            this.faceLandmarkInterface = this.retrofit.create(FaceLandmarkInterface.class);
        }
        return this.faceLandmarkInterface;
    }

    /**
     *
     * Makes a REST call to the server to fetch the data points for a given page
     *
     *
     * @param pageNumber
     */
    public void fetchImagesForPage(int pageNumber){

        HashMap<String,Object> request = new HashMap<>();
        request.put("db", fetchingDbName);
        request.put("collection", fetchingCollectionName);
        request.put("page_no",pageNumber);
        request.put("page_size",pageSize);

        Call<FaceLandmarkResponse> call = getApiInterface().loadDataForPage(request);
        call.enqueue(new Callback<FaceLandmarkResponse>() {
            @Override
            public void onResponse(Call<FaceLandmarkResponse> call,final Response<FaceLandmarkResponse> response) {
                processResponse(response);
            }

            @Override
            public void onFailure(Call<FaceLandmarkResponse> call, Throwable t) {

            }
        });
    }

    /**
     *
     * Called to process the API response, received from the server.
     * This is a collection of all the data points from the db
     *
     * @param response
     */
    private void processResponse(final Response<FaceLandmarkResponse> response){
        for(final FaceLandmarkResponse.FaceLandMarkDataPoint dataPoint : response.body().data) {
            Runnable worker = new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.d(TAG, "run: starting  "+dataPoint.image_url);
                        URL url = new URL(dataPoint.image_url);
                        Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                        imageProcessor.process(image, null, new com.google.firebase.samples.apps.mlkit.java.retrofitClient.utils.Callback() {
                            @Override
                            public void onCompleteProcessing(HashMap<String, Object> landmarks) {
                                Log.d(TAG, "onCompleteProcessing: Landmarks "+landmarks);
                                sendNetworkResponse(dataPoint.image_url,landmarks);
                            }

                            @Override
                            public void onFailure() {
                                Log.d(TAG, "onFailure: Unable to completeProcessing");
                            }
                        });
                    }  catch (Exception e){
                        e.printStackTrace();
                    }
                }
            };
            addCallable(worker);
        }

        //This is a forceful call to cancel all the threads which we weren't able to process
        //and couldn't be classified.
        //This also increments the current page and makes a call to start processing the next page.
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    sleep(2500);

                    cancelAllTasks();

                    currentPage++;

                    fetchImagesForPage(currentPage);

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
    }


    // Add a callable to the queue, which will be executed by the next available thread in the pool
    public void addCallable(Runnable runnable){
        synchronized (this) {
            Future future = executorService.submit(runnable);
            mRunningTaskList.add(future);
        }
    }

    /**
     *  Remove all tasks in the queue and stop all running threads
     *
     *  This is called after 2.500 seconds , forcefully, since the threads are running stale and are
     *  not terminating or giving an error code.
     */
    public void cancelAllTasks() {
        synchronized (this) {
            taskQueue.clear();
            for (Future task : mRunningTaskList) {
                if (!task.isDone()) {
                    task.cancel(true);
                }
            }
            mRunningTaskList.clear();
        }
    }


    /**
     *
     * Sends the landmarks along with the image_url, which was processed.
     *
     * @param image_url Image url processed
     * @param landmarks The landmarks received
     */
    private void sendNetworkResponse(String image_url, HashMap<String, Object> landmarks) {

        HashMap<String,Object> postBody = new HashMap<>();
        postBody.put("box",landmarks.get("BOX"));

        postBody.put("url",image_url);


        postBody.put("landmarks",new JSONObject(landmarks));
        postBody.put("db",dumpingDbName);
        postBody.put("collection",dumpingCollectionName);
        Log.d(TAG, "sendNetworkResponse: Values to be pushed "+postBody);

        Log.d(TAG, "sendNetworkResponse: Values to be pushed "+landmarks.get("BOX"));
        Log.d(TAG, "sendNetworkResponse: Data points pushed "+dataPointPushed.incrementAndGet() + " current page "+currentPage);


        getApiInterface().pushLandMarksForUrl(postBody).enqueue(new Callback<FaceLandmarkResponse>() {
            @Override
            public void onResponse(Call<FaceLandmarkResponse> call, Response<FaceLandmarkResponse> response) {

            }

            @Override
            public void onFailure(Call<FaceLandmarkResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: Unable to send network response "+t.toString());
            }
        });
    }

    private String returnStringFormat(HashMap<String, Float> boxCoordinates) {
        String toString=  "[(" + boxCoordinates.get("left")+","+boxCoordinates.get("top")+"),("
                + boxCoordinates.get("right")+","+boxCoordinates.get("top")+"),("
                + boxCoordinates.get("right")+","+boxCoordinates.get("bottom")+"),("
                + boxCoordinates.get("left")+","+boxCoordinates.get("bottom")+")]";
        return toString;
    }


    public void processImages(final Context context, final String... urls){
        for(final String imageUrl: urls){
            Runnable worker = new Runnable() {
                @Override
                public void run() {
                    try {
                        Log.d(TAG, "run: starting  "+imageUrl);
                        URL url = new URL(imageUrl);
                        Bitmap image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                        imageProcessor.process(image, null, new com.google.firebase.samples.apps.mlkit.java.retrofitClient.utils.Callback() {
                            @Override
                            public void onCompleteProcessing(HashMap<String, Object> landmarks) {
                                Log.d(TAG, "onCompleteProcessing: Landmarks "+landmarks);
                                writeToFile(landmarks,context,imageUrl);
                            }

                            @Override
                            public void onFailure() {
                                Log.d(TAG, "onFailure: Unable to completeProcessing");
                            }
                        });
                    }  catch (Exception e){
                        e.printStackTrace();
                    }
                }
            };
            addCallable(worker);
        }
    }

    private void writeToFile(HashMap<String,Object> data, Context context,String imageUrl) {
        try {
            File path = context.getFilesDir();
            File file = new File(path, "datadump"+Math.round(Math.random()*10000)+".txt");
            FileOutputStream stream = new FileOutputStream(file);
            try {
                stream.write(("Processed image "+imageUrl).getBytes());

                for(String key:data.keySet()){
                    stream.write(("Key : "+key+" = "+(data.get(key))).getBytes());
                    Log.d(TAG, "writeToFile: Values  Key : "+key+" = "+(data.get(key)));
                    stream.write("\n".getBytes());
                }

            } finally {
                stream.close();
            }
        }
        catch (Exception e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


}
