package com.google.firebase.samples.apps.mlkit.java.retrofitClient.utils;

import java.util.HashMap;

public abstract class Callback {

    abstract public void onCompleteProcessing(final HashMap<String,Object> landmarks);

    abstract public void onFailure();

}
