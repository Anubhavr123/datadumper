package com.google.firebase.samples.apps.mlkit.java.retrofitClient.utils;

import android.util.Log;

import java.util.concurrent.ThreadFactory;

public class BackgroundThreadFactory  implements ThreadFactory {
    private static int sTag = 1;
    private static String TAG = "BackgroundThreadFactory";

    @Override
    public Thread newThread(Runnable runnable) {
        Thread thread = new Thread(runnable);
        thread.setName("CustomThread" + sTag + "."+ Math.round(Math.random()*1000));

        // A exception handler is created to log the exception from threads
        thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable ex) {
                Log.e(TAG, thread.getName() + " encountered an error: " + ex.getMessage());
            }
        });
        return thread;
    }
}