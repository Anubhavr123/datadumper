package com.google.firebase.samples.apps.mlkit.java.facedetection;

import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.tasks.Task;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.common.FirebaseVisionPoint;
import com.google.firebase.ml.vision.face.FirebaseVisionFace;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceContour;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetector;
import com.google.firebase.ml.vision.face.FirebaseVisionFaceDetectorOptions;
import com.google.firebase.samples.apps.mlkit.common.CameraImageGraphic;
import com.google.firebase.samples.apps.mlkit.common.FrameMetadata;
import com.google.firebase.samples.apps.mlkit.common.GraphicOverlay;
import com.google.firebase.samples.apps.mlkit.java.VisionProcessorBase;
import com.google.firebase.samples.apps.mlkit.java.retrofitClient.clients.FaceLandmarkClient;
import com.google.firebase.samples.apps.mlkit.java.retrofitClient.interfaces.FaceLandmarkInterface;
import com.google.firebase.samples.apps.mlkit.java.retrofitClient.utils.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static com.google.firebase.ml.vision.face.FirebaseVisionFaceContour.ALL_POINTS;
import static com.google.firebase.ml.vision.face.FirebaseVisionFaceContour.LEFT_EYE;

/**
 * Face Contour Demo.
 */
public class FaceContourDetectorProcessor extends VisionProcessorBase<List<FirebaseVisionFace>> {

    private static final String TAG = "FaceContourDetectorProc";

    private final FirebaseVisionFaceDetector detector;

    public FaceContourDetectorProcessor() {
        FirebaseVisionFaceDetectorOptions options =
                new FirebaseVisionFaceDetectorOptions.Builder()
                        .setPerformanceMode(FirebaseVisionFaceDetectorOptions.FAST)
                        .setContourMode(FirebaseVisionFaceDetectorOptions.ALL_CONTOURS)
                        .build();

        detector = FirebaseVision.getInstance().getVisionFaceDetector(options);
    }

    @Override
    public void stop() {
        try {
            detector.close();
        } catch (IOException e) {
            Log.e(TAG, "Exception thrown while trying to close Face Contour Detector: " + e);
        }
    }

    @Override
    protected Task<List<FirebaseVisionFace>> detectInImage(FirebaseVisionImage image) {
        return detector.detectInImage(image);
    }

    @Override
    protected void onSuccess(
            @Nullable Bitmap originalCameraImage,
            @NonNull List<FirebaseVisionFace> faces,
            @NonNull FrameMetadata frameMetadata,
            @NonNull GraphicOverlay graphicOverlay,@NonNull Callback callback) {
        //graphicOverlay.clear();
        if (originalCameraImage != null) {
            CameraImageGraphic imageGraphic = new CameraImageGraphic(graphicOverlay, originalCameraImage);
            //graphicOverlay.add(imageGraphic);
        }
        for (int i = 0; i < faces.size(); ++i) {
            FirebaseVisionFace face = faces.get(i);
            FaceContourGraphic faceGraphic = new FaceContourGraphic(face);
            HashMap<String,Object> landmarks = new HashMap<>();

            //Returns landmarks in Object format
//            landmarks.put("ALL_POINTS",face.getContour(FirebaseVisionFaceContour.ALL_POINTS).getPoints().toString());
//            landmarks.put("FACE",face.getContour(FirebaseVisionFaceContour.FACE).getPoints().toString());
//            landmarks.put("LEFT_EYEBROW_TOP",face.getContour(FirebaseVisionFaceContour.LEFT_EYEBROW_TOP).getPoints().toString());
//            landmarks.put("LEFT_EYEBROW_BOTTOM",face.getContour(FirebaseVisionFaceContour.LEFT_EYEBROW_BOTTOM).getPoints().toString());
//            landmarks.put("RIGHT_EYEBROW_TOP",face.getContour(FirebaseVisionFaceContour.RIGHT_EYEBROW_TOP).getPoints().toString());
//            landmarks.put("RIGHT_EYEBROW_BOTTOM",face.getContour(FirebaseVisionFaceContour.RIGHT_EYEBROW_BOTTOM).getPoints().toString());
//            landmarks.put("LEFT_EYE",face.getContour(FirebaseVisionFaceContour.LEFT_EYE).getPoints().toString());
//            landmarks.put("RIGHT_EYE",face.getContour(FirebaseVisionFaceContour.RIGHT_EYE).getPoints().toString());
//            landmarks.put("UPPER_LIP_TOP",face.getContour(FirebaseVisionFaceContour.UPPER_LIP_TOP).getPoints().toString());
//            landmarks.put("UPPER_LIP_BOTTOM",face.getContour(FirebaseVisionFaceContour.UPPER_LIP_BOTTOM).getPoints().toString());
//            landmarks.put("LOWER_LIP_TOP",face.getContour(FirebaseVisionFaceContour.LOWER_LIP_TOP).getPoints().toString());
//            landmarks.put("LOWER_LIP_BOTTOM",face.getContour(FirebaseVisionFaceContour.LOWER_LIP_BOTTOM).getPoints().toString());
//            landmarks.put("NOSE_BRIDGE",face.getContour(FirebaseVisionFaceContour.NOSE_BRIDGE).getPoints().toString());
//            landmarks.put("NOSE_BOTTOM",face.getContour(FirebaseVisionFaceContour.NOSE_BOTTOM).getPoints().toString());

            //Returns in Python readable format
            try {

                landmarks.put("ALL_POINTS", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.ALL_POINTS).getPoints())));
                landmarks.put("FACE", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.FACE).getPoints())));
                landmarks.put("LEFT_EYEBROW_TOP", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.LEFT_EYEBROW_TOP).getPoints())));
                landmarks.put("LEFT_EYEBROW_BOTTOM", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.LEFT_EYEBROW_BOTTOM).getPoints())));
                landmarks.put("RIGHT_EYEBROW_TOP", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.RIGHT_EYEBROW_TOP).getPoints())));
                landmarks.put("RIGHT_EYEBROW_BOTTOM", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.RIGHT_EYEBROW_BOTTOM).getPoints())));
                landmarks.put("LEFT_EYE", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.LEFT_EYE).getPoints())));
                landmarks.put("RIGHT_EYE", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.RIGHT_EYE).getPoints())));
                landmarks.put("UPPER_LIP_TOP", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.UPPER_LIP_TOP).getPoints())));
                landmarks.put("UPPER_LIP_BOTTOM", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.UPPER_LIP_BOTTOM).getPoints())));
                landmarks.put("LOWER_LIP_TOP", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.LOWER_LIP_TOP).getPoints())));
                landmarks.put("LOWER_LIP_BOTTOM", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.LOWER_LIP_BOTTOM).getPoints())));
                landmarks.put("NOSE_BRIDGE", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.NOSE_BRIDGE).getPoints())));
                landmarks.put("NOSE_BOTTOM", new JSONArray(returnStringFormat(face.getContour(FirebaseVisionFaceContour.NOSE_BOTTOM).getPoints())));
                landmarks.put("BOX",(faceGraphic.getBoxCoordinates(face)));

//                landmarks.put("ALL_POINTS", returnStringFormat(face.getContour(FirebaseVisionFaceContour.ALL_POINTS).getPoints()));
//                landmarks.put("FACE", returnStringFormat(face.getContour(FirebaseVisionFaceContour.FACE).getPoints()));
//                landmarks.put("LEFT_EYEBROW_TOP", returnStringFormat(face.getContour(FirebaseVisionFaceContour.LEFT_EYEBROW_TOP).getPoints()));
//                landmarks.put("LEFT_EYEBROW_BOTTOM", returnStringFormat(face.getContour(FirebaseVisionFaceContour.LEFT_EYEBROW_BOTTOM).getPoints()));
//                landmarks.put("RIGHT_EYEBROW_TOP", returnStringFormat(face.getContour(FirebaseVisionFaceContour.RIGHT_EYEBROW_TOP).getPoints()));
//                landmarks.put("RIGHT_EYEBROW_BOTTOM", returnStringFormat(face.getContour(FirebaseVisionFaceContour.RIGHT_EYEBROW_BOTTOM).getPoints()));
//                landmarks.put("LEFT_EYE", returnStringFormat(face.getContour(FirebaseVisionFaceContour.LEFT_EYE).getPoints()));
//                landmarks.put("RIGHT_EYE", returnStringFormat(face.getContour(FirebaseVisionFaceContour.RIGHT_EYE).getPoints()));
//                landmarks.put("UPPER_LIP_TOP", returnStringFormat(face.getContour(FirebaseVisionFaceContour.UPPER_LIP_TOP).getPoints()));
//                landmarks.put("UPPER_LIP_BOTTOM", returnStringFormat(face.getContour(FirebaseVisionFaceContour.UPPER_LIP_BOTTOM).getPoints()));
//                landmarks.put("LOWER_LIP_TOP", returnStringFormat(face.getContour(FirebaseVisionFaceContour.LOWER_LIP_TOP).getPoints()));
//                landmarks.put("LOWER_LIP_BOTTOM", returnStringFormat(face.getContour(FirebaseVisionFaceContour.LOWER_LIP_BOTTOM).getPoints()));
//                landmarks.put("NOSE_BRIDGE", returnStringFormat(face.getContour(FirebaseVisionFaceContour.NOSE_BRIDGE).getPoints()));
//                landmarks.put("NOSE_BOTTOM", returnStringFormat(face.getContour(FirebaseVisionFaceContour.NOSE_BOTTOM).getPoints()));
//                landmarks.put("BOX",(faceGraphic.getBoxCoordinates(face)));

            } catch ( Exception e) {
                e.printStackTrace();
            }

            //Callback for after processing the images
            callback.onCompleteProcessing(landmarks);

        }
    }


    @Override
    protected void onFailure(@NonNull Exception e) {
        Log.d(TAG, "Face detection failed " + e);
    }

    private String returnStringFormat(List<FirebaseVisionPoint> points){
        String toString ="[";

        for(FirebaseVisionPoint point:points){
            toString += "{x:"+ point.getX()+", y:"+point.getY()+"},";
        }

        Log.d(TAG, "returnStringFormat: ");
        return toString.substring(0,toString.length()-1)+"]";
    }
}
