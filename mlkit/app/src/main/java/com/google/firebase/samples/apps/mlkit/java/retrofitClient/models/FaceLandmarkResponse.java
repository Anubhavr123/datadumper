package com.google.firebase.samples.apps.mlkit.java.retrofitClient.models;

import androidx.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FaceLandmarkResponse {

    public class FaceLandMarkDataPoint {
        @SerializedName("url")
        public String image_url;
        @SerializedName("data_category")
        public String data_category;

        public String getImage_url() {
            return image_url;
        }

        public void setImage_url(String image_url) {
            this.image_url = image_url;
        }

        public String getData_category() {
            return data_category;
        }

        public void setData_category(String data_category) {
            this.data_category = data_category;
        }

        @NonNull
        @Override
        public String toString() {
            return "FaceLandMarkDataPoint = [ image_url "+image_url+", data_category "+data_category+" ]";
        }
    }

    @SerializedName("data")
    public List<FaceLandMarkDataPoint> data;

    @NonNull
    @Override
    public String toString() {
        return data.toString();
    }
}
